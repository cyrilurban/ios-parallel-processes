/**
 * @file	projekt2.c
 * @author	CYRIL URBAN
 * @date:	2016-04-28
 * @brief	The roller coaster problem of synchronize parallel processes
 * 		using semaphores.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <semaphore.h>

#define serial_numberSIZE sizeof(int)
#define P_SIZE sizeof(int)
#define C_SIZE sizeof(int)
#define PT_SIZE sizeof(int)
#define RT_SIZE sizeof(int)
#define replationSIZE sizeof(int)
#define shmSIZE sizeof(sem_t)

// global share variables:
int *serial_number; // first column
int *P; // number of passenger
int *C; // number of car capacity
int *PT; // max time to generating new passanger
int *RT; // max time to route passage
int *replation; // replation car

FILE * fp; // proj2.out

// processes:
pid_t generatorPID;
pid_t carPID;
pid_t passengerPID;

// semaphores:
sem_t *sem_serial_number;
sem_t *sem_C_loading;
sem_t *sem_full_car;
sem_t *sem_empty_car;
sem_t *sem_C_unloading;
sem_t *sem_finished;

// logic of separate passenger(his ID)
void passenger (int passengerID) {

	// started
	sem_wait(sem_serial_number);
	fprintf(fp, "%d\t: P %d\t: started\n", ++(*serial_number), passengerID);
	fflush(fp);
	sem_post(sem_serial_number);

	//wait on car loading
	sem_wait(sem_C_loading);

	sem_wait(sem_serial_number);
	fprintf(fp, "%d\t: P %d\t: board\n", ++(*serial_number), passengerID);
	fflush(fp);
	sem_post(sem_serial_number);
	*replation = *replation + 1;

	if (*replation == *C) {

		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: P %d\t: board last\n", ++(*serial_number), passengerID);
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_full_car);
	}
	else
	{
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: P %d\t: board order %d\n", ++(*serial_number), passengerID, *replation);
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_C_loading);
	}


	//wait on car unloading
	sem_wait(sem_C_unloading);

	sem_wait(sem_serial_number);
	fprintf(fp, "%d\t: P %d\t: unboard\n", ++(*serial_number), passengerID);
	fflush(fp);
	sem_post(sem_serial_number);
	*replation = *replation - 1;

	if (*replation == 0) {
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: P %d\t: unboard last\n", ++(*serial_number), passengerID);
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_empty_car);
	}
	else
	{
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: P %d\t: unboard order %d\n", ++(*serial_number), passengerID, (*C - *replation));
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_C_unloading);
	}


	// wait on other, to same finish
	sem_wait(sem_finished);
	sem_wait(sem_serial_number);
	fprintf(fp, "%d\t: P %d\t: finished\n", ++(*serial_number), passengerID);
	fflush(fp);
	sem_post(sem_serial_number);
	sem_post(sem_finished);

	//usleep(500);
	exit(0);
}

// car logic
void car () {

	//started
	fprintf(fp, "%d\t: C 1\t: started\n", ++(*serial_number));
	fflush(fp);
	sem_post(sem_serial_number);

	for (int i = 0; i < (*P / *C); i++)
	{
		// load
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: C 1\t: load\n", ++(*serial_number));
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_C_loading);

		// wait on full car
		sem_wait(sem_full_car);

		// run
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: C 1\t: run\n", ++(*serial_number));
		fflush(fp);
		sem_post(sem_serial_number);

		// round time - sleep
		usleep(rand() % (*RT + 1));

		//unload
		sem_wait(sem_serial_number);
		fprintf(fp, "%d\t: C 1\t: unload\n", ++(*serial_number));
		fflush(fp);
		sem_post(sem_serial_number);
		sem_post(sem_C_unloading);

		// wait on empty car
		sem_wait(sem_empty_car);
	}

	// finish (car finish first - then passenger)
	sem_wait(sem_serial_number);
	fprintf(fp, "%d\t: C 1\t: finished\n", ++(*serial_number));
	fflush(fp);
	sem_post(sem_serial_number);
	sem_post(sem_finished);

	exit(0);
}

// elaboration input parametr and initialization share memmory
int init_share_memmory(int argc, char *argv[]) {

	serial_number = (int*)mmap(NULL, serial_numberSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	P = (int*)mmap(NULL, P_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	C = (int*)mmap(NULL, C_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	PT = (int*)mmap(NULL, PT_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	RT = (int*)mmap(NULL, RT_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	replation = (int*)mmap(NULL, replationSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

	*serial_number = 0;
	*P = 0;
	*C = 0;
	*PT = 0;
	*RT = 0;
	*replation = 0;

	// error number of arguments
	if (argc != 5) {
		return 1;
	}

	// error - char in any arguments
	char *is_char;

	*P = strtol(argv[1], &is_char, 10);
	if (*is_char != '\0') {
		return 2;
	}

	*C = strtol(argv[2], &is_char, 10);
	if (*is_char != '\0') {
		return 2;
	}

	*PT = strtol(argv[3], &is_char, 10);
	if (*is_char != '\0') {
		return 2;
	}

	*RT = strtol(argv[4], &is_char, 10);
	if (*is_char != '\0') {
		return 2;
	}

	// error - limits
	if (*P < 1 || *C < 1 || *PT < 0 || *PT > 5000 || *RT < 0 || *RT > 5000) {
		return 3;
	}

	// error - not a multiple
	if (*P % *C != 0) {
		return 4;
	}

	// else - not error -> return 0
	return 0;
}

// destroy (munmap share memmory)
void destroy_share_memmory() {
	munmap(serial_number, serial_numberSIZE);
	munmap(P, P_SIZE);
	munmap(C, C_SIZE);
	munmap(PT, PT_SIZE);
	munmap(RT, RT_SIZE);
	munmap(replation, replationSIZE);
}

// initialization all semaphores
void init_semaphore() {

	sem_serial_number = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_serial_number, 1, 0);

	sem_C_loading = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_C_loading, 1, 0);

	sem_full_car = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_full_car, 1, 0);

	sem_empty_car = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_empty_car, 1, 0);

	sem_C_unloading = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_C_unloading, 1, 0);

	sem_finished = mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	sem_init(sem_finished, 1, 0);
}

// destroy and munmap all semaphores
void destroy_semaphore() {

	sem_destroy(sem_serial_number);
	munmap(sem_serial_number, shmSIZE);

	sem_destroy(sem_C_loading);
	munmap(sem_C_loading, shmSIZE);

	sem_destroy(sem_full_car);
	munmap(sem_full_car, shmSIZE);

	sem_destroy(sem_empty_car);
	munmap(sem_empty_car, shmSIZE);

	sem_destroy(sem_C_unloading);
	munmap(sem_C_unloading, shmSIZE);

	sem_destroy(sem_finished);
	munmap(sem_finished, shmSIZE);
}

// main function - creat processes and call functions
int main(int argc, char *argv[]) {

	setbuf(stdout, NULL);

	// init share memmory and semaphore
	if (init_share_memmory(argc, argv) != 0) {
		fprintf(stderr, "Invalid argument\n");
		fclose(fp);
		destroy_share_memmory();
		return 1;
	}

	init_semaphore();
	fp = fopen ("proj2.out", "w+");

	// creat process - generator (passengers)
	if ((generatorPID = fork()) < 0) {
		fprintf(stderr, "fork error\n");
		exit(2);
	}
	if (generatorPID == 0) {
		// creat processes - generating separate passengers
		for (int i = 1; i <= *P ; i++) {

			// usleep - pause generating new process on random time (from interval <0;*PT>)
			usleep(rand() % (*PT + 1));

			if ((passengerPID = fork()) < 0) {
				destroy_share_memmory();
				destroy_semaphore();
				fprintf(stderr, "fork error\n");
				fclose(fp);
				exit(2);
				return 1;
			}
			if (passengerPID == 0) { // separate passenger
				passenger(i); // his ID
			}
		}

		exit(0);
		// creat process - car
	} else {
		if ((carPID = fork()) < 0) {
			destroy_share_memmory();
			destroy_semaphore();
			fprintf(stderr, "fork error\n");
			fclose(fp);
			exit(2);
			return 1;
		}
		if (carPID == 0) {
			car();
		}
	}

	// pockame az vsichni skonci
	waitpid(generatorPID, NULL, 0);
	waitpid(carPID, NULL, 0);
	waitpid(passengerPID, NULL, 0);

	// zrusime zdroje
	destroy_share_memmory();
	destroy_semaphore();
	fclose(fp);

	return 0;
}